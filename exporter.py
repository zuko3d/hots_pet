import requests as rq
from bs4 import BeautifulSoup
import tqdm
import time
import json

raw_players = open("player.json", 'r', encoding='utf-8').read()
split_players = raw_players.split('}')

pages_done = 1

for page_no, p in tqdm.tqdm(enumerate(split_players)):
    if page_no < pages_done:
        print(f"Skipping page {page_no} as it's processed already")
        continue
    if p.startswith('set()'):
        p = p[5:]
    players = eval(p + '}')
    stats = {}
    fout = open(f"page_{page_no}", 'w', encoding='utf-8')
    for player in tqdm.tqdm(players):
        if isinstance(player, tuple) and len(player) == 2:
            try:
                result = rq.get(f"https://api.hotslogs.com/Public/Players/{player[0]}/{player[1].replace('#', '_')}")
                while result.status_code != 200:
                    print('.', end='')
                    time.sleep(4.0)
                    result = rq.get(
                        f"https://api.hotslogs.com/Public/Players/{player[0]}/{player[1].replace('#', '_')}")
                if len(result.text) > 10:
                    player_id = json.loads(result.text)['PlayerID']

                    result = rq.get(f"https://www.hotslogs.com/Player/Profile?PlayerID={player_id}")
                    while result.status_code != 200:
                        print('.', end='')
                        time.sleep(4.0)
                        result = rq.get(f"https://www.hotslogs.com/Player/Profile?PlayerID={player_id}")

                    page = BeautifulSoup(result.text, "html.parser")
                    tables = page.find_all('table', attrs={'class': 'rgMasterTable'})
                    heroes = None
                    for table in tables:
                        if 'CharacterStatistics' in table['id']:
                            heroes = table
                            break
                    if heroes is not None:
                        heroes = heroes.find('tbody').find_all('tr')
                        hero_stats = {}
                        for hero in heroes:
                            tds = hero.find_all('td')
                            charname = tds[2].text
                            char_level = tds[3].text
                            games_played = tds[4].text
                            winrate = tds[6].text
                            hero_stats[charname] = [games_played, winrate, char_level]
                        fout.write(json.dumps([player_id, player[0], player[1], hero_stats]))
                        fout.write('$')
                        fout.flush()
                    else:
                        print(f"Failed to load info for {player}")
            except:
                print(f"ERROR occured! Skipping {player}")
        else:
            print("Skipping bad 'player'")
    fout.close()
