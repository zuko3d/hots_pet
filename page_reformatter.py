import glob
import tqdm
import numpy as np
import h5py

pages = glob.glob("E:\Projects\hots_pet\data\data\*")

heroes = ['Abathur', 'Alarak', 'Alexstrasza', 'Ana', "Anub'arak", 'Artanis', 'Arthas', 'Auriel', 'Azmodan', 'Blaze',
          'Brightwing', 'Cassia', 'Chen', 'Cho', 'Chromie', 'D.Va', 'Deckard', 'Dehaka', 'Diablo', 'E.T.C.', 'Falstad',
          'Fenix', 'Gall', 'Garrosh', 'Gazlowe', 'Genji', 'Greymane', "Gul'dan", 'Hanzo', 'Illidan', 'Jaina', 'Johanna',
          'Junkrat', "Kael'thas", "Kel'Thuzad", 'Kerrigan', 'Kharazim', 'Leoric', 'Li Li', 'Li-Ming', 'Lt. Morales',
          'Lunara', 'Lúcio', 'Maiev', 'Malfurion', 'Malthael', 'Medivh', 'Muradin', 'Murky', 'Nazeebo', 'Nova',
          'Probius', 'Ragnaros', 'Raynor', 'Rehgar', 'Rexxar', 'Samuro', 'Sgt. Hammer', 'Sonya', 'Stitches', 'Stukov',
          'Sylvanas', 'Tassadar', 'The Butcher', 'The Lost Vikings', 'Thrall', 'Tracer', 'Tychus', 'Tyrael', 'Tyrande',
          'Unknown', 'Uther', 'Valeera', 'Valla', 'Varian', 'Xul', 'Zagara', 'Zarya', 'Zeratul', "Zul'jin"]

pos_by_name = {
    name: i
    for i, name in enumerate(heroes)
}

games_played = np.zeros(shape=(200000, len(heroes)), dtype=np.int32)
levels = np.zeros(shape=(200000, len(heroes)), dtype=np.int32)
winrates = np.zeros(shape=(200000, len(heroes)), dtype=np.float32)

player_idx = 0
for filename in tqdm.tqdm(pages):
    file = open(filename, 'r')
    datas = file.read()
    players = datas.split('$')
    for player in players:
        if len(player) > 3:
            info = eval(player)
            if len(info) == 0:
                print(f"Bad player {info[1]}")
                player_idx -= 1
            else:
                for hero, stats in info[3].items():
                    pos = pos_by_name[hero]
                    games_played[player_idx, pos] = int(stats[0].replace(',', ''))
                    if len(stats[2]) > 0:
                        levels[player_idx, pos] = int(stats[2])
                    if '%' in stats[1]:
                        winrates[player_idx, pos] = float(stats[1][:-2]) / 100.0
            player_idx += 1

games_played = games_played[:player_idx]
levels = levels[:player_idx]
winrates = winrates[:player_idx]

print(f"Total players: {player_idx}")

out = h5py.File('out.h5', 'w')
out.create_dataset('games_played', data=games_played)
out.create_dataset('levels', data=levels)
out.create_dataset('winrates', data=winrates)

out.close()
