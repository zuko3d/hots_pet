import requests as rq
import tqdm
import json
import time
import copy

all_players = set()
fout = open("player.json", 'w', encoding='utf-8')
sleep_time = 4.0

result = rq.get(f"http://hotsapi.net/api/v1/replays/",
                params={'with_players': 'true', 'start_date': '2016-05-01', 'end_date': '2016-12-01'})
replays = json.loads(result.text)

for page in tqdm.trange(100):
    prev_players = copy.copy(all_players)
    result = rq.get(f"http://hotsapi.net/api/v1/replays/",
                    params={'with_players': 'true', 'start_date': '2015-01-01', 'end_date': '2015-05-01'})
    while result.status_code != 200:
        time.sleep(sleep_time)
        result = rq.get(f"http://hotsapi.net/api/v1/replays/paged", params={'with_players': 'true', 'page': page})

    replays = json.loads(result.text)['replays']
    for rep in replays:
        for player in rep['players']:
            all_players.add((rep['region'], player['battletag']))
    new_players = all_players - prev_players
    print(len(new_players))
    fout.write(str(new_players))

fout.close()
