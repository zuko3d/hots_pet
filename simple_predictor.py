import numpy as np
import h5py
import tqdm
from keras.models import Sequential, load_model
from keras.layers import Dense, InputLayer, LeakyReLU, Dropout, BatchNormalization
from keras.callbacks import ReduceLROnPlateau, EarlyStopping

from sklearn.preprocessing import StandardScaler, MinMaxScaler

pure_data = h5py.File('out.h5', 'r')

games_played = pure_data['games_played'][:].astype(np.float32)
levels = pure_data['levels'][:].astype(np.float32)
winrates = pure_data['winrates'][:].astype(np.float32)

heroes_total = games_played.shape[-1]

games_played[:, 70] = 0
levels[:, 70] = 0
winrates[:, 70] = 0

pure_data.close()

# gpl = games_played.sum(axis=1)
# for i in range(0, 101, 10):
#     print(f"{i}:\t{np.percentile(gpl, i)}")

# prepare input dataset ==================
# for j in tqdm.trange(7):
top_x = 5
print(f"Preparing dataset, top_x = {top_x}")

x_train = np.zeros(shape=(len(games_played), heroes_total * 2), dtype=np.float32)
y_train = np.zeros_like(x_train, dtype=np.float32)

idx = 0

for games, winrate in zip(games_played, winrates):
    if games.sum() > 600:
        flat_indices = np.argsort(games)[-top_x:]
        max_games = games[flat_indices[-1]]
        total_games = games.sum()
        # games_normalized = np.log(games + 1)

        coef = 1.0 - 1.0 / (1.0 + 0.25 * games)
        wrt = 0.5 * (1.0 - coef) + winrate * coef

        x_train[idx, flat_indices] = games[flat_indices] / max_games
        x_train[idx, flat_indices + heroes_total] = wrt[flat_indices]

        y_train[idx][:heroes_total] = games / max_games
        y_train[idx][heroes_total:] = wrt
        idx += 1

x_train = x_train[:idx]
y_train = y_train[:idx]
# ========================================

input_scaler = StandardScaler()
out_scaler = StandardScaler()

x_train = input_scaler.fit_transform(x_train)
y_train = out_scaler.fit_transform(y_train)

learn = True

if learn == True:
    model = Sequential()
    model.add(InputLayer(input_shape=(x_train.shape[-1],)))

    # model.add(Dense(heroes_total * 20))
    # # model.add(BatchNormalization())
    # model.add(LeakyReLU())
    #
    # # model.add(Dropout(0.25))
    #
    # model.add(Dense(heroes_total * 15))
    # model.add(LeakyReLU())
    # model.add(Dropout(0.15))
    #
    # model.add(Dense(heroes_total * 12))
    # model.add(BatchNormalization())
    # model.add(LeakyReLU())
    # model.add(Dropout(0.05))
    #
    # model.add(Dense(heroes_total * 6))
    # model.add(BatchNormalization())
    # model.add(LeakyReLU())
    # model.add(Dropout(0.35))
    #
    model.add(Dense(heroes_total * 4))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.05))

    model.add(Dense(heroes_total * 4))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.05))

    model.add(Dense(heroes_total * 3))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.05))

    model.add(Dense(heroes_total * 3))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.05))
    # model.add(Dense(heroes_total * 2))
    # model.add(LeakyReLU())

    # model.add(Dropout(0.05))
    model.add(Dense(y_train.shape[-1], activation='sigmoid'))
    # model.add(Dense(y_train.shape[-1], activation='linear'))

    model.compile(loss='mse', optimizer='adamax')

    ror = ReduceLROnPlateau(verbose=True, patience=4, factor=0.5)
    early_stopper = EarlyStopping(patience=10, min_delta=1e-4)

    history = model.fit(
        x=x_train,
        y=y_train,
        batch_size=10000,
        epochs=1000,
        verbose=2,
        validation_split=0.05,
        callbacks=[ror, early_stopper],
    )

    model.save("model.hdf5", overwrite=True)

    print(history.history['val_loss'])
else:
    model = load_model("model.hdf5")

heroes = ['Abathur', 'Alarak', 'Alexstrasza', 'Ana', "Anub'arak", 'Artanis', 'Arthas', 'Auriel', 'Azmodan', 'Blaze',
          'Brightwing', 'Cassia', 'Chen', 'Cho', 'Chromie', 'D.Va', 'Deckard', 'Dehaka', 'Diablo', 'E.T.C.', 'Falstad',
          'Fenix', 'Gall', 'Garrosh', 'Gazlowe', 'Genji', 'Greymane', "Gul'dan", 'Hanzo', 'Illidan', 'Jaina', 'Johanna',
          'Junkrat', "Kael'thas", "Kel'Thuzad", 'Kerrigan', 'Kharazim', 'Leoric', 'Li Li', 'Li-Ming', 'Lt. Morales',
          'Lunara', 'Lúcio', 'Maiev', 'Malfurion', 'Malthael', 'Medivh', 'Muradin', 'Murky', 'Nazeebo', 'Nova',
          'Probius', 'Ragnaros', 'Raynor', 'Rehgar', 'Rexxar', 'Samuro', 'Sgt. Hammer', 'Sonya', 'Stitches', 'Stukov',
          'Sylvanas', 'Tassadar', 'The Butcher', 'The Lost Vikings', 'Thrall', 'Tracer', 'Tychus', 'Tyrael', 'Tyrande',
          'Unknown', 'Uther', 'Valeera', 'Valla', 'Varian', 'Xul', 'Zagara', 'Zarya', 'Zeratul', "Zul'jin"]


my_vector = np.zeros(shape=(1, heroes_total * 2,))
my_vector[0, heroes_total:] = 0.5

# from hots logs
my_vector[0, heroes.index('Brightwing')] = 1.0
my_vector[0, heroes.index('Nova')] = 0.625
my_vector[0, heroes.index('Zagara')] = 0.62
my_vector[0, heroes.index('Valla')] = 0.55
my_vector[0, heroes.index('Malfurion')] = 0.375


my_vector[0, heroes.index('Brightwing') + heroes_total] = 0.55
my_vector[0, heroes.index('Nova') + heroes_total] = 0.46
my_vector[0, heroes.index('Zagara') + heroes_total] = 0.588
my_vector[0, heroes.index('Valla') + heroes_total] = 0.489
my_vector[0, heroes.index('Malfurion') + heroes_total] = 0.6

# from HotS
my_vector[0, heroes.index('Brightwing')] = 1.0  # * 102
my_vector[0, heroes.index('Nova')] = 0.99  # * 102
my_vector[0, heroes.index('Zagara')] = 0.81  # * 102
my_vector[0, heroes.index('Valla')] = 0.79  # * 102
my_vector[0, heroes.index('Malfurion')] = 0.44  # * 102

my_vector[0, heroes.index('Brightwing') + heroes_total] = 0.621
my_vector[0, heroes.index('Nova') + heroes_total] = 0.461
my_vector[0, heroes.index('Zagara') + heroes_total] = 0.58
my_vector[0, heroes.index('Valla') + heroes_total] = 0.544
my_vector[0, heroes.index('Malfurion') + heroes_total] = 0.545

# from HotS
# my_vector[0, heroes.index('Brightwing')] = 1.0  # * 102
# my_vector[0, heroes.index('Nova')] = 0
# my_vector[0, heroes.index('Zagara')] = 0
# my_vector[0, heroes.index('Valla')] = 0
# my_vector[0, heroes.index('Malfurion')] = 0
#
# my_vector[0, heroes.index('Brightwing') + heroes_total] = 0.5
# my_vector[0, heroes.index('Nova') + heroes_total] = 0.5
# my_vector[0, heroes.index('Zagara') + heroes_total] = 0.5
# my_vector[0, heroes.index('Valla') + heroes_total] = 0.5
# my_vector[0, heroes.index('Malfurion') + heroes_total] = 0.5

# Shkulev
# my_vector[0, heroes.index('Li Li')] = 1.0  # * 102
# my_vector[0, heroes.index('Li Li') + heroes_total] = 0.59
#
# my_vector[0, heroes.index('Chromie')] = 0.92  # * 102
# my_vector[0, heroes.index('Chromie') + heroes_total] = 0.461
#
# my_vector[0, heroes.index('Stukov')] = 0.625  # * 102
# my_vector[0, heroes.index('Stukov') + heroes_total] = 0.615
#
# my_vector[0, heroes.index('Valla')] = 0.3  # * 102
# my_vector[0, heroes.index('Valla') + heroes_total] = 0.58
#
# my_vector[0, heroes.index("Gul'dan")] = 0.25  # * 102
# my_vector[0, heroes.index("Gul'dan") + heroes_total] = 0.45

predicted = model.predict(input_scaler.transform(my_vector))
predicted = out_scaler.inverse_transform(predicted)[0]

print("By games played:")
order = np.argsort(predicted[:heroes_total])
for pos in order[-20:]:
    print(f"{heroes[pos]:15}:\t{predicted[pos]:.3}\t{predicted[pos + heroes_total]*100:.3} %")

print("By winrate:")
order = np.argsort(predicted[heroes_total:])
for pos in order[-5:]:
    print(f"{heroes[pos]:15}:\t{predicted[pos]:.3}\t{predicted[pos + heroes_total]*100:.3} %")
